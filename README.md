---
title: README
description: 
published: true
date: 2022-10-27T01:11:13.232Z
tags: 
editor: markdown
dateCreated: 2022-10-22T02:37:02.344Z
---

# wiki

## Name
Abfelbaums wiki

## Description
This is the repo that contains the wiki content from https://wiki.abfelbaum.dev

## Usage
Visit https://wiki.abfelbaum.dev and have fun reading!

## Support
Please report any issues regarding the wiki to reply+abfelbaum-tools-wiki-9-issue-@git.abfelbaum.dev

## Contributing
I'm open to any contributions! 

Please send me an email (address can be found under support) if you found something that you want to improve by yourself. I'll then create an user account for you so you can create a merge request.

I'm sorry for this complicated process. The reason for that is that my last GitLab instance, that didn't have restrictions on who can register / log in via SSO, suddenly had over 5000 users and very much ads in there.
