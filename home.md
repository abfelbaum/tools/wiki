---
title: Hi!
description: 
published: true
date: 2022-10-23T19:29:37.397Z
tags: 
editor: markdown
dateCreated: 2022-10-22T01:33:33.869Z
---

I'm Abfelbaum and this is my wiki.

This wiki is mostly for myself but kind internet strangers are allowed to read it too.

Have a look around. Maybe you'll find something interesting!

If you have any issues or questions have a look at the [GitLab project](https://git.abfelbaum.dev/abfelbaum/tools/wiki)s README.