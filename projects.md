---
title: Projects
description: 
published: true
date: 2022-10-27T00:50:10.001Z
tags: 
editor: markdown
dateCreated: 2022-10-22T02:00:49.514Z
---

> Since I am currently developing, implementing and improving my workflows some things may not be as expected
{.is-warning}

All of my projects are free open source software and can be found on [my GitLab](https://git.abfelbaum.dev/explore),

More information on how I work with these projects can be found [here](/projects/general-information)

# Projects

## Tools

### AbfiBin

[Repository](https://git.abfelbaum.dev/abfelbaum/tools/abfibin)
[Website](https://bin.abfelbaum.dev)
[Documentation](/projects/abfibin/start)


A pastebin alternative with support for multiple files per bin and a CLI. Authentication via ZITADEL is required.


### wiki

[Repository](https://git.abfelbaum.dev/abfelbaum/tools/wiki)
[Website](https://wiki.abfelbaum.dev)

This wiki. Please report issues to this wiki via the provided mail address.

## Bots

### TelegramLabelBot
[Repository](https://git.abfelbaum.dev/abfelbaum/bots/telegramlabelbot)

This is a bot for adding labels (admin titles) to users in Telegram groups. 

It was intended for being used as a pronoun bot in some queer groups. Therefore some strings are still referring to that topic, but they can easily be replaced in the source code.

### WhiteListBot
[Repository](https://git.abfelbaum.dev/abfelbaum/bots/telegramwhitelistbot)

A simple Telegram bot for whitelisting users on a Minecraft server.

It requires [ServerTap](https://github.com/phybros/servertap) installed and configured on the server.

### AtAdminBot
[Repository](https://git.abfelbaum.dev/abfelbaum/bots/atadminbot)
[@AtAdminBot on Telegram](https://t.me/atadminbot)

A simple Telegram bot for notifying the admins of a group.

## Libraries

### Abfelbaum.Telegram.Bot.Framework

A more or less complete framework for working with Telegram in C#. Based on [Telegram.Bot](https://github.com/TelegramBots/Telegram.Bot) and still not released, but highly functional.

## Other

### Infrastructure

The setup of my infrastructure

### Images

[Documentation](/projects/images)
[GitLab](https://git.abfelbaum.dev/abfelbaum/images)

Docker images for me to simplify my life