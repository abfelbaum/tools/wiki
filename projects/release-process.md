---
title: Release process
description: 
published: true
date: 2022-10-24T16:19:34.443Z
tags: 
editor: markdown
dateCreated: 2022-10-23T23:21:36.352Z
---

When a new version of an application has to be published some steps have to be performed so that can run smoothly.

## Prerequisites

### Commit messages

Commit messages should, to contribute to the automatically generated `CHANGELOG.md`, follow the [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) specification. A quick summary can be found [here](https://www.conventionalcommits.org/en/v1.0.0/#summary). 
Messages that do not follow that specification will be ignored.

### Configure `semantic-release`

I use `semantic-release` for generating a `CHANGELOG.md`, determining the next version and pushing the corresponding git tag.

I use the following configuration, which is put in `release.config.js`:

```js
const branch = process.env.CI_COMMIT_BRANCH

const config = {
    tagFormat: "${version}",
    branches: [
        "main",
        {
            name: "dev",
            channel: "default",
            prerelease: "beta"
        },
        {
            name: "*",
            channel: "default",
            prerelease: "alpha"
        }
    ],
    plugins: [
        ["@semantic-release/commit-analyzer", {
            preset: "conventionalcommits"
        }],
        ["@semantic-release/release-notes-generator", {
            preset: "conventionalcommits"
        }],
    ]
}

if (config.branches.some(it => it === branch || (it.name === branch && !it.prerelease))) {
    
    config.plugins.push([ "@semantic-release/changelog", {
        changelogTitle: "# Changelog\n\nAll notable changes to this project will be documented in this file. See\n[Conventional Commits](https://conventionalcommits.org) for commit guidelines."
    }]);
    
    config.plugins.push([ "@semantic-release/git", {
        message: "chore(release): ${nextRelease.version}\n\n${nextRelease.notes}"
    }]);
}

module.exports = config
```

The `main` branch is treated as the stable release branch, the `dev` branch as the beta release branch and any other branch as an alpha release branch.

To use this configuration in your project add

```yml
stages:
  - release

include:
  - project: 'abfelbaum/ci'
    ref: main
    file: 'release.yml'
```
([source](https://git.abfelbaum.dev/abfelbaum/ci/-/blob/main/release.yml))

to the `.gitlab-ci.yml`.

Any release process in the CI then can run with the `only: tags` requirement.

## Creating a release

Creating a release is reaaaaally simple. 

The CI configuration exposes a button to run `semantic-release` for every commit.
Activating this button triggers a pipeline that:

* Analyzes the commits
* Determines the next version
* (only stable) Generates a `CHANGELOG.md` in the format of [keep a changelog](https://keepachangelog.com/en/1.0.0/)
* Creates and publishes a git tag

Anything else may, or may be not, run autmatically. That depends on the project.
