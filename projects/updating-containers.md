---
title: Update containers
description: 
published: true
date: 2022-10-24T17:00:30.600Z
tags: 
editor: markdown
dateCreated: 2022-10-23T23:55:54.676Z
---

Docker containers should be updated now and then to make use of the security updates implemented in the base images.

## Updating

To implement that in your project with GitLab CI just add the following snippet to your `.gitlab-ci.yml`:

```yml
stages:
  - update

include:
  - project: 'abfelbaum/ci'
    ref: main
    file: 'containers/update-trigger.yml'
```
[source](https://git.abfelbaum.dev/abfelbaum/ci/-/blob/main/containers/update-trigger.yml)

This snippet does the following:

* Creates a job when the CI is triggered by a schedule
* Gets all git tags that are in the semantic versioning pattern of stable releases (i.e. without `-beta`, only `1.0.0`), sorts them ascending and creates a new pipeline for them to update the containers
* Sets the env variable `$CI_CONTAINER_UPDATE` to true on all update jobs
* Sets the env variable `$CI_CONTAINER_UPDATE_LATEST` to true on the update job with the latest version
* Adds rules for easier job selection

## Rules

There are some predefined rules that can be referenced to integrate your release pipeline with this snippet easily.

### `.is_release`

The rule checks if a tag name matches the semantic versioning pattern of stable releases (i.e. without `-beta`, only `1.0.0`.

### `.is_preview`

The rule does the same as `.is_release` but allows prereleases (i.e. with `-beta`, so `1.0.0-beta`) too.

### `.is_container_update`

The rule checks if this is a update pipeline triggered by the snippet. Use this rule only on jobs that are required for updating the container (i. e. building or pushing)

### `.is_container_update_old`

The rule does the same as `.is_container_update` but also checks if `$CI_CONTAINER_UPDATE_LATEST` is `false`. That results in execution only for the releases that are not the latest ones.

### `.is_container_update_latest`

The rule does the same as `.is_container_update` but only for the newest version. This can be used for creating the `:latest` tag of a container or redeploying your application.

## Examples

View the full CI at [AbfiBin GitLab repository](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/-/blob/main/.gitlab-ci.yml)

```yml
publish-server:
  stage: publish
  only: 
    - tags
  dependencies:
    - build-server
  image: registry.git.abfelbaum.dev/abfelbaum/images/waypoint:latest
  services:
    - name: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/docker:dind
      alias: docker
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - waypoint init
    - waypoint build
  artifacts:
    public: false
    paths:
      - data.db

publish-server-latest:
  stage: publish
  rules:
   - !reference [.is_release_stable, rules]
   - !reference [.is_container_update_latest, rules]
  dependencies:
    - publish-server
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/docker:latest  
  services:
    - name: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/docker:dind
      alias: docker
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script: 
    - docker pull "$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG"
    - docker image tag "$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG" "$CI_REGISTRY_IMAGE:latest"
    - docker push "$CI_REGISTRY_IMAGE:latest"

deploy-server-testing:
  stage: deploy
  rules: 
    - !reference [.is_release_preview, rules]
    - !reference [.is_container_update_latest, rules]
  dependencies:
    - publish-server
  image: registry.git.abfelbaum.dev/abfelbaum/images/waypoint:latest
  script:
    - waypoint deploy
  environment:
    name: AbfiBin Testing
    deployment_tier: testing
    url: https://testing.bin.abfelbaum.dev

deploy-server-production:
  stage: deploy
  dependencies:
    - publish-server
  rules:
    - !reference [.is_release_stable, rules]
    - !reference [.is_container_update_latest, rules]
  image: registry.git.abfelbaum.dev/abfelbaum/images/waypoint:latest
  script:
    - waypoint deploy
  environment:
    name: AbfiBin
    deployment_tier: production
    url: https://bin.abfelbaum.dev
```

