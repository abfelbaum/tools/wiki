---
title: Docker images
description: 
published: true
date: 2022-10-23T19:26:35.275Z
tags: 
editor: markdown
dateCreated: 2022-10-23T19:26:35.275Z
---

I create my own docker images for usage in my projects. Most of that is just retagging other images.

That way I have control over properties of my images that I wouldn't have otherwise. Also I won't hit remote registries rate limits because they are hosted on my own GitLab instance.

They can be found [here](https://git.abfelbaum.dev/abfelbaum/images).

Please do, if you want to use them, build them for yourself. I most likely will remove some images and tags when I don't need them anymore and that is going to break your projects.

## dotnet

These are the docker images for the dotnet versions I'm using. Currently (and most likely in the future too) that are the `sdk`, `aspnet` and `runtime` images with the [currently supported .NET releases](https://dotnet.microsoft.com/en-us/platform/support/policy/dotnet-core) that I am using.

## gitlab-org

These are docker images, made by gitlab, that I want to retag. Currently the release-cli is mirrored because it is required for creating releases from a pipeline.

## semantic-release

[Link](https://github.com/semantic-release/semantic-release)

This is the docker image I use to generate release tags and changelogs. It comes preinstalled with the `semantic-release` cli and the plugins I require.

## waypoint

[Link](https://www.waypointproject.io/)

Hashicorp Waypoint is used for building and deploying my projects. There is just one image tag, `latest`, with the latest waypoint version.