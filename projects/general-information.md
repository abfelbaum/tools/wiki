---
title: General project information
description: 
published: true
date: 2022-10-24T00:00:46.336Z
tags: 
editor: markdown
dateCreated: 2022-10-23T01:50:01.893Z
---

Here are some guidelines that are valid for any of my projects

## Contributing

Everyone is invited to create issues.

Due to some bot created spam on my old GitLab instance it is not possible to register at my current GitLab instance. Issues can be created via GitLab ServiceDesk emails (view READMEs). If you want/need an account to contribute code changes feel free to mention that in a mail to the ServiceDesk. I'll create an account for you.

## Project status

Every of my projects, that is not archived is still maintained by me.

Most projects of mine do not have a continous flow of issues to be worked on since they are fairly small projects. Due to that fact there may have not been some commits or releases for a long time (AtAdminBot hasn't seen a new commit for a year for example).

## License

Maybe I forgot to add a license to a project. My general license guidelines are:

* [AGPL v3](https://choosealicense.com/licenses/agpl-3.0/) for server projects 
* [GPL v3](https://choosealicense.com/licenses/gpl-3.0/) for client projects
* [LGPL v3](https://choosealicense.com/licenses/lgpl-3.0/) for libraries
* [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) for non programming stuff like configuration files, documentation or this wiki

## Docker images / containers

I provide docker images for most of my projects. Here are some guidelines for these so you know what you're working with:

* Released containers are rebuilt every 24 hours
	* This does not account for containers containing prereleases! 
* Released containers won't be removed from the registry
  * This does not account for containers containing prereleases. They will be deleted after 90 days!
* `latest` tags always point to the newest image version

Containers with prereleases can be recognized by alpha, beta or rc tags.

Releases:
* `1.0.0`
* `0.1.0`
* `10.2.5`

Prereleases:
* `1.0.0-beta`
* `0.1.0-alpha.35`
* `10.2.5-rc.2`

[More information on how containers are updated](/projects/updating-containers)

## Release cycles

My projects do not follow any specific release cycles. Normally I'll just release something when I feel like that moment would be a good moment. 

[More information on how releases are created](/projects/release-process)

## Versioning

* My projects use [semantic versioning](https://semver.org/)
* Prereleases may not be stable and are subject to change